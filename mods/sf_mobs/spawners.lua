local S = minetest.get_translator("sf_mobs")
local EDITOR = minetest.settings:get_bool("sf_editor", false) or minetest.settings:get_bool("creative_mode", false)

local SPAWNER_DETECT_RANGE = 20
local SPAWNER_TIMER_MIN = 4
local SPAWNER_TIMER_MAX = 6
local SPAWNER_PREVENT_RANGE = 1
local SPAWNER_LIMIT_RANGE = 32
local SPAWNER_RESTART_TIMER = 1

local register_mob_spawner = function(id, def)
	local drawtype, pointable
	if EDITOR then
		drawtype = "allfaces"
		pointable = true
	else
		drawtype = "airlike"
		pointable = false
	end
	minetest.register_node("sf_mobs:spawner_"..id, {
		description = def.description,
		pointable = pointable,
		drawtype = drawtype,
		visual_scale = 0.5,
		paramtype = "light",
		sunlight_propagates = true,
		tiles = {def.texture.."^sf_mobs_spawner_overlay.png"},
		walkable = false,
		groups = { spawner = 1, editor_breakable = 1 },
		on_timer = function(pos)
			if EDITOR then
				return
			end
			if def.limit == 0 then
				return
			end
			local objs = minetest.get_objects_inside_radius(pos, math.max(SPAWNER_PREVENT_RANGE, SPAWNER_LIMIT_RANGE))
			local mobs_of_same_kind = 0
			for o=1, #objs do
				local obj = objs[o]
				local opos = obj:get_pos()
				if vector.distance(opos, pos) <= SPAWNER_PREVENT_RANGE and obj:is_player() then
					local timer = minetest.get_node_timer(pos)
					timer:start(SPAWNER_RESTART_TIMER)
					return
				end
				local lua = obj:get_luaentity()
				if lua then
					local kind = lua.name
					if vector.distance(opos, pos) <= SPAWNER_LIMIT_RANGE and kind == "sf_mobs:"..id then
						mobs_of_same_kind = mobs_of_same_kind + 1
					end
				end
				if mobs_of_same_kind > def.limit-1 then
					local timer = minetest.get_node_timer(pos)
					timer:start(SPAWNER_RESTART_TIMER)
					return
				end
			end

			local node = minetest.get_node(pos)
			local def = minetest.registered_nodes[node.name]
			if def and def._on_spawn then
				def._on_spawn(pos)
			end
		end,
		_on_spawn = function(pos)
			minetest.add_entity(pos, "sf_mobs:"..id)
			minetest.log("action", "[sf_mobs] Mob spawner at "..minetest.pos_to_string(pos).." spawns mob of type 'sf_mobs:"..id.."'")
		end,
	})
end

register_mob_spawner("crawler", {
	description = S("Crawler Spawner"),
	texture = "sf_mobs_shadow.png",
	limit = 20,
})
register_mob_spawner("flyershooter", {
	description = S("Flyer Shooter Spawner"),
	texture = "sf_mobs_shadow.png",
	limit = 12,
})
register_mob_spawner("shadow_orb", {
	description = S("Shadow Orb Spawner"),
	texture = "sf_mobs_shadow.png",
	-- this mob must be spawned manually
	limit = 0,
})

local spawner_check_timer = 0
minetest.register_globalstep(function(dtime)
	if EDITOR then
		return
	end
	spawner_check_timer = spawner_check_timer + dtime
	if spawner_check_timer < 0.5 then
		return
	end
	spawner_check_timer = 0
	local players = minetest.get_connected_players()
	for p=1, #players do
		local player = players[p]
		local ppos = player:get_pos()
		local offset = vector.new(SPAWNER_DETECT_RANGE, SPAWNER_DETECT_RANGE, SPAWNER_DETECT_RANGE)
		local spawners = minetest.find_nodes_in_area(vector.add(ppos, offset), vector.subtract(ppos, offset), "group:spawner")
		for s=1, #spawners do
			local spawner = spawners[s]
			local timer = minetest.get_node_timer(spawner)
			if not timer:is_started() then
				local time = SPAWNER_TIMER_MIN + (math.random(0, 1000) * (SPAWNER_TIMER_MAX - SPAWNER_TIMER_MIN)) / 1000
				timer:start(time)
			end
		end
	end
end)
