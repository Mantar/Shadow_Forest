sf_zones = {}

local EDITOR = minetest.settings:get_bool("sf_editor", false) or minetest.settings:get_bool("creative_mode", false)
local GRAVITY = 9.81
local ZONE_CHECK_TIME = 0.7
local CRYSTALS_TO_BREAK_BARRIER = 3
local zone_timer = 0.0

local registered_zones = {}

--[[
name: zone name
def: {
	pos_min, -- left bottom front corner
	pos_max, -- right top back corner
	on_enter(zonename, player), -- optional function, called when player enters zone
	on_leave(zonename, player), -- optional function, called when player leaves zone
}
]]
function sf_zones.register_zone(name, def)
	local zoneinfo = table.copy(def)
	zoneinfo._players_in_zone = {}
	registered_zones[name] = zoneinfo
end

function sf_zones.get_zone(name)
	return registered_zones[name]
end

function sf_zones.in_which_zones(pos)
	local zones_list = {}
	for zonename, _ in pairs(registered_zones) do
		if sf_zones.is_in_zone(pos, zonename) then
			table.insert(zones_list, zonename)
		end
	end
	return zones_list
end

function sf_zones.is_in_zone(pos, zonename)
	local zone = sf_zones.get_zone(zonename)
	if not zone then
		return false
	end
	local pos_min = zone.pos_min
	local pos_max = zone.pos_max
	return pos.x >= pos_min.x and pos.y >= pos_min.y and pos.z >= pos_min.z and pos.x <= pos_max.x and pos.y <= pos_max.y and pos.z <= pos_max.z
end

local check_zones = function(player)
	local pos = player:get_pos()
	local pname = player:get_player_name()
	for zonename, zone in pairs(registered_zones) do
		if sf_zones.is_in_zone(pos, zonename) then
			if not zone._players_in_zone[pname] then
				zone._players_in_zone[pname] = true
				minetest.log("action", "[sf_zones] "..player:get_player_name().." enters zone '"..zonename.."'")
				if zone.on_enter then
					zone.on_enter(zonename, player)
				end
			end
		else
			if zone._players_in_zone[pname] then
				zone._players_in_zone[pname] = nil
				minetest.log("action", "[sf_zones] "..player:get_player_name().." leaves zone '"..zonename.."'")
				if zone.on_leave then
					zone.on_leave(zonename, player)
				end
			end
		end
	end
end

minetest.register_globalstep(function(dtime)
	zone_timer = zone_timer + dtime
	if zone_timer < ZONE_CHECK_TIME then
		return
	end
	zone_timer = 0

	local players = minetest.get_connected_players()
	for p=1, #players do
		local player = players[p]
		check_zones(player)
	end
end)

function sf_zones.report_location_change(player)
	check_zones(player)
end

sf_zones.register_zone("portal_tree", {
	pos_min = vector.new(315, 45, 385),
	pos_max = vector.new(333, 95, 407),
	on_enter = function(zonename, player)
		if EDITOR then
			return
		end
		sf_music.change_music(player, "shadow_forest")
		sf_sky.set_sky(player, "storm_clouds")
		sf_dialog.show_dialog(player, "intro2", true)
	end,
})
sf_zones.register_zone("dead_forest", {
	pos_min = vector.new(5, 31, 5),
	pos_max = vector.new(209, 100, 182),
	on_enter = function(zonename, player)
		if EDITOR then
			return
		end
		sf_music.change_music(player, "shadow_forest")
		sf_sky.set_sky(player, "smoky_clouds")
	end,
	on_leave = function(zonename, player)
		if EDITOR then
			return
		end
		sf_sky.set_sky(player, "storm_clouds")
	end,
})
sf_zones.register_zone("forest", {
	pos_min = vector.new(8, 31, 183),
	pos_max = vector.new(173, 80, 506),
	on_enter = function(zonename, player)
		if EDITOR then
			return
		end
		sf_music.change_music(player, "shadow_forest")
		sf_sky.set_sky(player, "storm_clouds")
	end,
	on_leave = function(zonename, player)
		if EDITOR then
			return
		end
		sf_sky.set_sky(player, "storm_clouds")
	end,
})

sf_zones.register_zone("fog_chasm", {
	pos_min = vector.new(212, 14, 2),
	pos_max = vector.new(277, 56, 177),
	on_enter = function(zonename, player)
		if EDITOR then
			return
		end
		sf_music.change_music(player, "fog_chasm")
		sf_sky.set_sky(player, "fog")
	end,
	on_leave = function(zonename, player)
		if EDITOR then
			return
		end
		sf_sky.set_sky(player, "storm_clouds")
	end,
})
sf_zones.register_zone("snow_mountain", {
	pos_min = vector.new(279, 45, 17),
	pos_max = vector.new(433, 100, 167),
	on_enter = function(zonename, player)
		if EDITOR then
			return
		end
		sf_music.change_music(player, "shadow_forest")
		sf_sky.set_sky(player, "storm_clouds")
	end,
	on_leave = function(zonename, player)
		if EDITOR then
			return
		end
		sf_sky.set_sky(player, "storm_clouds")
	end,
})
sf_zones.register_zone("chimneys", {
	pos_min = vector.new(19, 42, 2),
	pos_max = vector.new(116, 78, 83),
	on_enter = function(zonename, player)
		sf_dialog.show_dialog(player, "chimneys", true)
	end,
})
local break_weak_spikeplants_in_area = function(pos_min, pos_max)
	sf_util.break_nodes_in_area(pos_min, pos_max, "sf_nodes:spikeplant_weak", {name = "sf_zones_spikeplant_break", gain=1.0})
end

sf_zones.register_zone("shadow_bush_barrier", {
	pos_min = vector.new(144, 48, 178),
	pos_max = vector.new(171, 61, 192),
	on_enter = function(zonename, player)
		local crystals = sf_resources.get_resource_count(player, "sf_resources:light_crystal")
		if crystals >= CRYSTALS_TO_BREAK_BARRIER then
			local zone = sf_zones.get_zone(zonename)
			minetest.after(2, function()
				break_weak_spikeplants_in_area(zone.pos_min, zone.pos_max)
			end)
			sf_dialog.show_dialog(player, "bush_spell", true)
		else
			sf_dialog.show_dialog(player, "bush_spell_early", true)
		end
	end,
})

sf_zones.register_zone("boss_arena", {
	pos_min = vector.new(52, 12, 20),
	pos_max = vector.new(76, 41, 64),
})

sf_zones.register_zone("boss_arena_enter", {
	pos_min = vector.new(52, 12, 40),
	pos_max = vector.new(76, 30, 64),
	on_enter = function(zonename, player)
		local zone = sf_zones.get_zone("boss_arena")
		local objs = minetest.get_objects_in_area(zone.pos_min, zone.pos_max)
		local boss_exists = false
		local pmeta = player:get_meta()
		if pmeta:get_int("sf_mobs:boss_defeated") == 1 then
			minetest.log("action", "[sf_zones] "..player:get_player_name().." entered boss arena but boss already defeated")
			return
		end
		for o=1, #objs do
			local lua = objs[o]:get_luaentity()
			if lua and lua.name == "sf_mobs:shadow_orb" then
				boss_exists = true
				break
			end
		end
		if not boss_exists then
			local spawners = minetest.find_nodes_in_area(zone.pos_min, zone.pos_max, "sf_mobs:spawner_shadow_orb")
			for s=1, #spawners do
				minetest.add_entity(spawners[s], "sf_mobs:shadow_orb")
				minetest.log("action", "[sf_zones] Shadow orb boss spawed at "..minetest.pos_to_string(spawners[s]))
			end
		end
		sf_dialog.show_dialog(player, "boss", true)
	end,
})



-- TODO: The music for the "shrine" zones is kinda hacky, this only
-- works as long these are embedded completely in another zone
sf_zones.register_zone("snow_mountain_shrine", {
	pos_min = vector.new(333, 60, 69),
	pos_max = vector.new(377, 94, 82),
	on_enter = function(zonename, player)
		if EDITOR then
			return
		end
		sf_music.change_music(player, "crystal")
	end,
	on_leave = function(zonename, player)
		if EDITOR then
			return
		end
		sf_music.change_music(player, "shadow_forest")
	end,
})
sf_zones.register_zone("fog_chasm_shrine", {
	pos_min = vector.new(316, 15, 20),
	pos_max = vector.new(348, 31, 48),
	on_enter = function(zonename, player)
		if EDITOR then
			return
		end
		sf_music.change_music(player, "crystal")
	end,
	on_leave = function(zonename, player)
		if EDITOR then
			return
		end
		sf_music.change_music(player, "shadow_forest")
	end,
})
sf_zones.register_zone("forest_shrine", {
	pos_min = vector.new(53, 64, 453),
	pos_max = vector.new(67, 76, 479),
	on_enter = function(zonename, player)
		if EDITOR then
			return
		end
		sf_music.change_music(player, "crystal")
	end,
	on_leave = function(zonename, player)
		if EDITOR then
			return
		end
		sf_music.change_music(player, "shadow_forest")
	end,
})
