local S = minetest.get_translator("sf_loot")
local EDITOR = minetest.settings:get_bool("sf_editor", false) or minetest.settings:get_bool("creative_mode", false)

local vase_drop = nil
if not EDITOR then
	vase_drop = ""
end

minetest.register_node("sf_loot:vase", {
	description = S("Vase"),
	tiles = {
		"sf_loot_vase_top.png",
		"sf_loot_vase_bottom.png",
		"sf_loot_vase_side.png",
	},
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{ -2/16, 4/16, -2/16, 2/16, 5/16, 2/16 }, -- cap top
			{ -3/16, 3/16, -3/16, 3/16, 4/16, 3/16 }, -- cap bottom
			{ -4/16, -6/16, -4/16, 4/16, 3/16, 4/16 }, -- main body
			{ -3/16, -7/16, -3/16, 3/16, -6/16, 3/16 }, -- base top
			{ -2/16, -0.5, -2/16, 2/16, -7/16, 2/16 }, -- base bottom
		},
	},
	sounds = {
		footstep = sf_sounds.node_sound_stone_defaults().footstep,
		dug = { name = "sf_loot_vase_break", gain = 0.5 },
	},
	groups = { editor_breakable = 1, loot_node = 1 },
	on_destruct = function(pos)
		if EDITOR then
			return
		end
		local node = minetest.get_node(pos)
		if node.param2 > 0 then
			for i=1, node.param2 do
				local obj = minetest.add_entity(pos, "sf_resources:healing_essence")
				if obj then
					local vel = vector.zero()
					vel.x = math.random(-100, 100)*0.01
					vel.y = math.random(0, 100)*0.01
					vel.z = math.random(-100, 100)*0.01
					obj:set_velocity(vel)
				end
			end
		end
	end,
	drop = vase_drop,
})
