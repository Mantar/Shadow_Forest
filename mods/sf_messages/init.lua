local S = minetest.get_translator("sf_messages")
sf_messages = {}

local current_messages = {}
local speech_queues = {}

local BIG_FONT_AT_WINDOW_WIDTH = 1920

sf_messages.is_showing_speech = function(player)
	local pname = player:get_player_name()
	return current_messages[pname] and current_messages[pname].speech_time ~= nil
end

sf_messages.remove_current_speech = function(from_player)
	local pname = from_player:get_player_name()
	local message = current_messages[pname]
	if not sf_messages.is_showing_speech(from_player) then
		return
	end
	from_player:hud_remove(message.speech_bg)
	from_player:hud_remove(message.speech_text)
	if message.speech_icon then
		from_player:hud_remove(message.speech_icon)
	end
	message.speech_bg = nil
	message.speech_text = nil
	message.speech_icon = nil
	message.speech_sound = nil
	message.speech_time = nil
	message.speech_duration = nil
end

sf_messages.show_speech = function(to_player, text, icon, sound, duration)
	local pname = to_player:get_player_name()
	if not current_messages[pname] then
		current_messages[pname] = {}
	end
	if sf_messages.is_showing_speech(to_player) then
		if not speech_queues[pname] then
			speech_queues[pname] = {}
		end
		local new_entry = { text, icon, sound, duration }
		table.insert(speech_queues[pname], new_entry)
		return
	end

	local id_bg = to_player:hud_add({
		hud_elem_type = "image",
		position = { x = 1, y = 0 },
		scale = { x = -40, y = 10 },
		text = "sf_messages_speech_bubble.png",
		alignment = { x = -1, y = 1 },
		offset = { x = -24, y = 24 },
		z_index = 100,
	})
	local id_icon
	if icon then
		id_icon = to_player:hud_add({
			hud_elem_type = "image",
			position = { x = 0.6, y = 0 },
			scale = { x = 2, y = 2 },
			text = "sf_messages_portrait_bg.png^("..icon..")",
			offset = { x = -6, y = 40 },
			alignment = { x = 1, y = 1 },
			z_index = 101,
		})
	end
	local text_size
	local window_info = minetest.get_player_window_information(to_player:get_player_name())
	if window_info and window_info.size and window_info.size.x >= BIG_FONT_AT_WINDOW_WIDTH then
		text_size = { x = 2, y = 2 }
	else
		text_size = { x = 1, y = 1 }
	end

	local id_text = to_player:hud_add({
		hud_elem_type = "text",
		position = { x = 0.6, y = 0 },
		scale = { x = 100, y = 100 },
		text = text,
		number = 0xFFFFFF,
		alignment = { x = 1, y = 1 },
		size = text_size,
		style = 0,
		offset = { x = 136, y = 34 },
		z_index = 102,
	})
	if sound then
		minetest.sound_play(sound, {to_player=to_player:get_player_name()}, true)
	end
	current_messages[pname].speech_bg = id_bg
	current_messages[pname].speech_icon = id_icon
	current_messages[pname].speech_sound = sound
	current_messages[pname].speech_text = id_text
	local now = minetest.get_us_time()
	current_messages[pname].speech_time = now
	current_messages[pname].speech_duration = duration
end


minetest.register_globalstep(function(dtime)
	local now = minetest.get_us_time()
	for playername, message in pairs(current_messages) do
		local player = minetest.get_player_by_name(playername)
		if player then
			if message.speech_duration and (now > (message.speech_time + message.speech_duration)) then
				sf_messages.remove_current_speech(player)
			end
		end
	end

	for playername, queue in pairs(speech_queues) do
		local player = minetest.get_player_by_name(playername)
		if player and (not sf_messages.is_showing_speech(player)) then
			if #queue >= 1 then
				local entry = queue[1]
				sf_messages.show_speech(player, entry[1], entry[2], entry[3], entry[4])
				table.remove(queue, 1)
			end
		end
	end

end)

minetest.register_on_leaveplayer(function(player)
	local pname = player:get_player_name()
	current_messages[pname] = nil
	speech_queues[pname] = nil
end)
